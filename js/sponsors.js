/*global $, console */

/**
 * This script is built for the page which lists sponsor logos.
 **/

// Load this script in footer and after the jQuery script so that 
// we don't have to use jQuery's document ready function
(function (window, document) {
    'use strict';

    var settings = {
            /*"data_src": "https://events.itnint.com/iw16/services/PartnerListingPublished.aspx",*/
            "data_src": "https://events.itnint.com/IW17/services/PartnersListingPublished.aspx",
            "test_data_src": "test.json",
            "maximumItemsPerRow": 4,
            "priorities": {
                "Diamond": 1,
                "Platinum": 2,
                "Gold": 3,
                "Silver": 4,
                "Silver Sp": 5,
                "Bronze": 6,
                "Cloud": 8,
                "Emerging": 10,
                "Default": 50
            },
            "lightbox": {
                "id": '#lightbox-content'
            }
        },
        data,
        filteredDataByType = {},
        filteredDataByPriority = {},
        itemsByIdCount = 0,
        itemsById = {},
        $lightbox = $(settings.lightbox.id),
        hasOwnProperty = Object.prototype.hasOwnProperty;

    function setPartnerTypePriority(partnerByType) {
        var priority;

        // Priority has only to be set once per type
        if (partnerByType.priority === undefined) {
            for (priority in settings.priorities) {
                if (settings.priorities.hasOwnProperty(priority) && partnerByType.title === priority) {
                    partnerByType.priority = settings.priorities[priority];

                    return;
                }
            }

            // Fallback: Default priority
            partnerByType.priority = settings.priorities.Default;
        }
    }

    /**
     * Test if item (logo) meets requirements to be displayed at all
     * @returns {boolean}
     */
    function meetsLogoRequirements(item) {
        if (item.PartnerType === "" ||
            item.LogoUrl === "") {
            return false;
        }

        return true;
    }

    function filterDataByType(data) {
        if (!meetsLogoRequirements(data)) {
            return;
        }

        var partnerTypeName = data.PartnerType,
            partnerId = data.PartnerId;

        // Test if the type exists, and add it to the object
        if (filteredDataByType[partnerTypeName] === undefined) {
            filteredDataByType[partnerTypeName] = {};
        }

        // Store data of each partnerType by partnerId
        filteredDataByType[partnerTypeName][partnerId] = data;

        // This is a starting point to create custom row titles
        if (filteredDataByType[partnerTypeName].title === undefined) {
            filteredDataByType[partnerTypeName].title = partnerTypeName;
        }

        // Count items
        if (filteredDataByType[partnerTypeName].itemsInLastRow === undefined) {
            filteredDataByType[partnerTypeName].itemsInLastRow = 0;
        }
        filteredDataByType[partnerTypeName].itemsInLastRow += 1;

        // If the maximum of items (four items) in a row is exceeded,
        // reset amount of items and increase row count
        if (filteredDataByType[partnerTypeName].rowCount === undefined) {
            filteredDataByType[partnerTypeName].rowCount = 1;
        }
        if (filteredDataByType[partnerTypeName].itemsInLastRow > settings.maximumItemsPerRow) {
            filteredDataByType[partnerTypeName].itemsInLastRow = 1;
            filteredDataByType[partnerTypeName].rowCount += 1;
        }
    }

    /*
    add a list of partner data to the partner object, and sort that list alphabetically. That list is what
    will be used to dislay the partner data in order
    */
    function alphabetize(prioritizedObjects) {
        //Get all rows into an array
        var retObjects = prioritizedObjects;

        for (var priorityKey in prioritizedObjects) {
            var arr = [];
            for (var partnerKey in prioritizedObjects[priorityKey]) {
                // add to array - if row is a a partnerkey
                if (/^[0-9]+$/.test(partnerKey)) {
                    arr.push(prioritizedObjects[priorityKey][partnerKey]);
                }
            }
            //sort the array
            arr.sort(function (a, b) {
                return (a.Company > b.Company) ? 1 : ((b.Company > a.Company) ? -1 : 0);
            });
            //add the array to the object
            retObjects[priorityKey].sortedObjList = arr;
        }
        return retObjects;
    }

    /**
     * Take objects of an object and sort them by the priority property.
     * @returns { Object } sortedObjects
     */
    function sortDataByPriority(objects) {
        var sortedPriorities = [],
            sortedObjects = {},
            object,
            i,
            l;

        // Create an array with all priorities
        for (object in objects) {
            if (objects.hasOwnProperty(object)) {
                sortedPriorities.push(objects[object].priority);
            }
        }

        // Sort all priorities in array
        sortedPriorities.sort(function (a, b) {
            return a - b;
        });

        // Build object with all objects ordered by priority
        for (i = 0, l = sortedPriorities.length; i < l; i += 1) {
            for (object in objects) {
                if (objects.hasOwnProperty(object) && objects[object].priority === sortedPriorities[i]) {
                    sortedObjects[sortedPriorities[i]] = objects[object];
                }
            }
        }

        var retObject = alphabetize(sortedObjects);
        return retObject;
    }

    /**
     * Create an extra object which contains all items.
     * This allows us to retrieve data from single items by their ID
     * (and we don't have to loop through the "filteredDataByType" object).
     */
    function storeIndividualItems(item) {
        itemsById[item.PartnerId] = item;
        itemsByIdCount += 1;
    }

    function filterData(data) {
        var items;
        //Making sure we have a json
        if (data !== undefined && data.items === undefined) {
            data = JSON.parse(data);
        }
        //Double checking it actually has data
        if (data !== undefined && data.items && data.items.item) {
            items = data.items.item;

            items.forEach(function (item) {
                storeIndividualItems(item);
                filterDataByType(item);

                // Give each partnerType a priority to make them sortable
                setPartnerTypePriority(filteredDataByType[item.PartnerType]);
            });
        } else {
            console.log("The received data does not have the required structure.");
        }
    }

    /**
     * Remove everything from the URL except for the domain name and ending
     * @returns {string} url
     */
    function getDomainName(url) {
        if (url !== undefined && url !== "") {
            var rule = /([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}/;
            url = rule.exec(url);
            if (url !== null) {
                url = url[0];
            }
        }
        return url;
    }

    function getLogoAltText(text) {
        if (text !== undefined && text !== "") {
            return "Logo: " + text;
        }
        return "";
    }

    function updateText($textcontainer, text) {
        if (text != undefined && text != null && text != "") {
            $textcontainer.text(text);
            $textcontainer.show();
        } else {
            $textcontainer.hide();
        }
    }

    function convertTextToArray(text) {
        var array = text.split(','),
            i,
            l;

        // Remove leading and trailing spaces
        for (i = 0, l = array.length; i < l; i += 1) {
            array[i] = array[i].trim();
        }
        return array;
    }

    /*
     * update the product and category lists in the lightbox. Only display the list ocntainer if there is data
     */
    function updateList($listcontainer, text) {
        if (text != undefined && text != null && text != "") {
            var $list = $listcontainer.find('ul'),
                listElements = convertTextToArray(text),
                $element,
                i = 0,
                l = listElements.length;

            // Remove all child nodes before we add new ones
            $list.empty();

            while (i < l) {
                $element = $('<li>' + listElements[i] + '</li>');
                $element.appendTo($list);
                i += 1;
            }
            $listcontainer.show();
        } else {
            $listcontainer.hide();
        }
    }

    /*
     * update the contacts in the lightbox. Only display the list ocntainer if there is data
     */
    function updateContacts($listcontainer, contactMap) {
        var $list = $listcontainer.find('ul'),
            $element;
        // Remove all child nodes before we add new ones
        $list.empty();

        if (contactMap["contact-name"] != undefined && contactMap["contact-name"] != null && contactMap["contact-name"] != "") {
            $element = $('<li>' + contactMap["contact-name"] + '</li>');
            $element.appendTo($list);
            if (contactMap["contact-title"] != undefined && contactMap["contact-title"] != null && contactMap["contact-title"] != "") {
                $element = $('<li>' + contactMap["contact-title"] + '</li>');
                $element.appendTo($list);
            }
            if (contactMap["contact-email"] != undefined && contactMap["contact-email"] != null && contactMap["contact-email"] != "") {
                //$element = $('<li>' + contactMap["contact-email"] + '</li>');
                $element = $('<li><a href="mailto:' + contactMap["contact-email"] + '" target="_top">' + contactMap["contact-email"] + '</a></li>');
                $element.appendTo($list);

            }
            $listcontainer.show();
        } else {
            $listcontainer.hide();
        }
    }

    /*
     * update the social icons lightbox. Only display the icon if there is data
     */
    function updateSocialIcon($iconcontainer, className, link) {
        if (link != undefined && link != null && link != "") {
            var trimmedName,
                $element = $iconcontainer.find(className);

            // Replace "." with space, and then remove leading and trailing spaces.
            // The two steps are useful in case the className consists of several names, such as ".name1.name2"
            trimmedName = className.replace(".", " ").trim();

            if ($element[0]) {
                $element.attr({
                    'class': trimmedName,
                    'href': link
                });
            } else {
                $element = $('<a class="' + trimmedName + '" href="' + link + '" target="_blank" />');
                $element.appendTo($iconcontainer);
            }
            $iconcontainer.find(className).show();
        } else {
            $iconcontainer.find(className).hide();
        }

    }


    /**
     * Here we return an object which contains several content types ("text", etc.).
     * Each content type can contain several properties.
     * A property consists of a name that matches an element's class name, and a value.
     * By using a property name that maches an element's name, we make it easier to
     * address that specific element.
     */
    function getLightboxContent(item) {
        return {
            "text": {
                ".sponsor-type": item.PartnerType,
                ".title": item.Company,
                ".website-name": getDomainName(item.Company_URL) || getDomainName(item.WebUrl),
                ".description": item.Guide_and_Website_Description
            },
            "list": {
                ".customer-solutions": item.Products_Supported,
                ".industry-segments": item.Industry_Segments
            },
            ".company-contact": {
                "contact-name": item.Company_Contact_Name,
                "contact-email": item.Company_Contact_Email,
                "contact-title": item.Company_Contact_Title
            },
            /* insure that icon data is validated. If not, then use empty string*/
            "social-icons": {
                ".linkedin": /^(@.+|https?:\/\/[a-z0-9\-\.]+)/.test(item.LinkedIn) ? item.LinkedIn : "",
                ".facebook": /^(@.+|https?:\/\/[a-z0-9\-\.]+)/.test(item.Facebook) ? item.Facebook : "",
                ".twitter": /^(@.+|https?:\/\/[a-z0-9\-\.]+)/.test(item.Twitter) ? item.Twitter : "",
                ".youtube": /^(@.+|https?:\/\/[a-z0-9\-\.]+)/.test(item.YouTube) ? item.YouTube : ""
            },

            // The following types match attribute names
            "src": {
                ".logo-src": item.LogoUrl
            },
            "alt": {
                ".logo-alt": getLogoAltText(item.Company)
            },
            "href": {
                ".website-href": item.Company_URL || item.WebUrl
            },
            "title": {
                ".website-title": item.Company
            }
        };
    }

    /**
     * Update text that is displyed in lightbox depending on selected logo
     */
    function beforeLightboxOpen($element) {
        var itemId = $element.attr('partner-id'),
            item = itemsById[itemId],
            contents = getLightboxContent(item),
            contentType,
            properties,
            propertyName,
            value,
            $socialIcons = $lightbox.find(".social-icons");

        for (contentType in contents) {
            if (contents.hasOwnProperty(contentType)) {
                properties = contents[contentType];

                for (propertyName in properties) {

                    if (properties.hasOwnProperty(propertyName)) {
                        value = properties[propertyName];

                        // Hide element if necessary information is missing
                        if (contentType !== "alt" && contentType !== "title") {
                            // Load information into existing elements
                            switch (contentType) {
                                case "text":
                                    updateText($lightbox.find(propertyName), value);
                                    break;

                                case "list":
                                    updateList($lightbox.find(propertyName), value);
                                    break;

                                case "social-icons":
                                    updateSocialIcon($socialIcons, propertyName, value);
                                    break;

                                default:
                                    // Default is used to update an element's attribute only
                                    $lightbox.find(propertyName).attr(contentType, value);
                            }
                        }
                    }
                }
                // Handled as a hashmap outside of loop instead of individual properties as
                // rendering is dependent on existence of all fields in hashmap
                if (contentType == ".company-contact") {
                    updateContacts($lightbox.find(contentType), contents[contentType]);
                }
            }
        }
    }

    /**
     * Add handler to element which tiggers lightbox using the defined settings.
     * For config options, see https://github.com/noelboss/featherlight/#configuration
     */
    function addLightboxHandler($element) {
        $element.featherlight({
            targetAttr: 'data-lightbox',
            beforeOpen: function () {
                beforeLightboxOpen($element);
            }
        });
    }

    /**
     * Test if item meets requirements for adding lightbox functionality
     * @returns {boolean}
     */
    function meetsLightboxRequirements(item) {
        if (item.PartnerType === "" ||
            item.Company === "" ||
            item.Company_URL === "" ||
            item.Guide_and_Website_Description === "") {
            return false;
        }

        return true;
    }

    /**
     * Create a single item
     * @returns {$element}
     */
    function createItem(item) {
        var newItemLink,
            newItemContent,
            newItem = $('<div>', {
                'class': 'pure-u-md-1-4 pure-u-1-2'
            });

        newItemContent = $('<img src="' + item.LogoUrl + '">');

        if (meetsLightboxRequirements(item)) {
            newItemLink = $('<a href="#" data-lightbox="' + settings.lightbox.id + '" partner-id="' + item.PartnerId + '">');
            addLightboxHandler(newItemLink);
            newItemLink.append(newItemContent);
            newItem.append(newItemLink);
        } else {
            newItem.append(newItemContent);
        }

        return newItem;
    }

    /**
     * Create items that are displayed in rows
     * @returns {integer} itemsCount
     */
    function createItems(items, $firstRow) {
        var item,
            itemsCount = 0,
            $newItem;

        items.forEach(function (item) {
            // Only display sponsors with a logo (logoUrl)
            if (item.LogoUrl !== "") {
                $newItem = createItem(item);
                $firstRow.append($newItem);

                itemsCount += 1;
            }
        });

        // Hide partner type title ("Platinum", etc.) and logo container if it doesn't contain logos
        if (itemsCount === 0) {
            $firstRow.addClass('hidden');
            $firstRow.prev().addClass('hidden');
        }

        return itemsCount;
    }

    /**
     * This extra row is the last row for each type of partner and might
     * contain less then the default four items in a row
     */
    function createExtraRow(itemsContainer, items, $firstRow, itemsCount) {
        var i,
            l,
            $extraRow,
            $itemToBeMoved;

        // Grab the last items and put them into an extra row to apply special styling
        if (itemsCount <= settings.maximumItemsPerRow && itemsContainer.itemsInLastRow > 0) {
            $firstRow.addClass('children-' + itemsContainer.itemsInLastRow);
        } else if (itemsCount > settings.maximumItemsPerRow && itemsContainer.itemsInLastRow > 0) {
            // Create extra row with specific CSS classes
            $extraRow = $('<div class="pure-g sponsor-logo-row children-' + itemsContainer.itemsInLastRow + '">');

            // For each item in extra row ...
            var nextRowNdx1 = items.length - itemsContainer.itemsInLastRow;
            i = nextRowNdx1;
            for (i; i < items.length; i += 1) {
                // ... take the item ...
                $itemToBeMoved = $firstRow.children().eq(nextRowNdx1)
                    // ... and move it into the extra row. Static ndx because this action removes it from $firstRow.children()
                $itemToBeMoved.appendTo($extraRow);
            }

            // Insert the extra row after the existing (first) row
            $extraRow.insertAfter($firstRow);
        }
    }


    function createRows(rows) {
        var logoContainer = $('#sponsor-logo-container'),
            row,
            $newRowsTitle,
            $newRows,
            $separator,
            itemsCount;

        for (var partnerPriority in rows) {
            // Add partner type title ("Platinum", etc.) to container
            $newRowsTitle = $('<h2 class="pure-u-1-1">' + rows[partnerPriority].title + '</h2>');
            $newRowsTitle.appendTo(logoContainer);

            // Create first row for one partner type
            $newRows = $('<div class="pure-g sponsor-logo-row">');
            $newRows.appendTo(logoContainer);

            itemsCount = createItems(rows[partnerPriority].sortedObjList, $newRows);
            createExtraRow(rows[partnerPriority], rows[partnerPriority].sortedObjList, $newRows, itemsCount);

            // This separator ensures that each row starts in a new line and
            // doesn't nest with the following rows which might not be 100% wide
            $separator = $('<div class="pure-u-1-1 separator">');
            $separator.appendTo(logoContainer);
        }

    }


    function getData(url) {
        var data = $.get(url, function (data) {})
            .done(function (data) {
                filterData(data);
                filteredDataByPriority = sortDataByPriority(filteredDataByType);
                createRows(filteredDataByPriority);
            })
            .fail(function () {
                console.log("Error while loading sponsor list.");
            })
            .always(function () {
                //                console.log("FILTERED DATA BY TYPES:");
                //                console.log(filteredDataByType);
                //                
                //                console.log("SORTED DATA BY PRIORITY:");
                //                console.log(filteredDataByPriority);
                //                
                //                console.log("ALL ITEMS BY ID:");
                //                console.log(itemsById);
            });

        return data;
    }

    function setupTestDataSrc() {
        if (document.location.hostname === "127.0.0.1" ||
            document.location.hostname === "localhost") {
            settings.data_src = settings.test_data_src;
        }
    }

    function init() {
        setupTestDataSrc();
        getData(settings.data_src);
    }

    init();

}(window, document));
